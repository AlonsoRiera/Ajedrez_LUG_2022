﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajedrez
{
    class Alfil : Pieza
    {
        public Alfil(bool Blanca)
        {
            this.Blanca = Blanca;
            if (this.Blanca)
            {
                this.Imagen = Ajedrez.Properties.Resources.wb;
            }
            else
            {
                this.Imagen = Ajedrez.Properties.Resources.bb;
            }
        }

        public override List<Casillero> PosiblesMovimientos(Casillero cas, List<Casillero> casilleros)
        {
            List<Casillero> posiblesmovimientos = new List<Casillero>();

            Casillero tempcas;
            bool piezaencontrada = false;
            int cont = 0;
            while (!piezaencontrada)
            {
                cont++;
                tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + cont) && casilla.X == (cas.X + cont) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
                if (tempcas != null)
                {
                    posiblesmovimientos.Add(tempcas);
                    if (tempcas.Pieza != null)
                    {
                        piezaencontrada = true;
                    }
                }
                else
                {
                    piezaencontrada = true;
                }
            }
            tempcas = null;
            piezaencontrada = false;
            cont = 0;
            while (!piezaencontrada)
            {
                cont++;
                tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - cont) && casilla.X == (cas.X - cont) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
                if (tempcas != null)
                {
                    posiblesmovimientos.Add(tempcas);
                    if (tempcas.Pieza != null)
                    {
                        piezaencontrada = true;
                    }
                }
                else
                {
                    piezaencontrada = true;
                }
            }
            tempcas = null;
            piezaencontrada = false;
            cont = 0;
            while (!piezaencontrada)
            {
                cont++;
                tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + cont) && casilla.X == (cas.X - cont) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
                if (tempcas != null)
                {
                    posiblesmovimientos.Add(tempcas);
                    if (tempcas.Pieza != null)
                    {
                        piezaencontrada = true;
                    }
                }
                else
                {
                    piezaencontrada = true;
                }
            }
            tempcas = null;
            piezaencontrada = false;
            cont = 0;
            while (!piezaencontrada)
            {
                cont++;
                tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - cont) && casilla.X == (cas.X + cont) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
                if (tempcas != null)
                {
                    posiblesmovimientos.Add(tempcas);
                    if (tempcas.Pieza != null)
                    {
                        piezaencontrada = true;
                    }
                }
                else
                {
                    piezaencontrada = true;
                }
            }


            return posiblesmovimientos.Where(x => x != null).ToList();
        }
    }
}