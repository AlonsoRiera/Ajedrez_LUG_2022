﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajedrez
{
    class Caballo : Pieza
    {
        public Caballo(bool Blanca)
        {
            this.Blanca = Blanca;
            if (this.Blanca)
            {
                this.Imagen = Ajedrez.Properties.Resources.caballo;
            }
            else
            {
                this.Imagen = Ajedrez.Properties.Resources.bn;
            }
        }

        public override List<Casillero> PosiblesMovimientos(Casillero cas, List<Casillero> casilleros)
        {
            List<Casillero> posiblesmovimientos = new List<Casillero>();
          
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 2) && casilla.X == (cas.X + 1) && (casilla.Pieza == null ||(casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca))));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 2) && casilla.X == (cas.X - 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca))));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 2) && casilla.X == (cas.X + 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca))));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 2) && casilla.X == (cas.X - 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca))));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == (cas.X + 2) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca))));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X + 2) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca))));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == (cas.X - 2) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca))));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X - 2) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca))));


            return posiblesmovimientos.Where(x => x != null).ToList();
        }
    }
}
