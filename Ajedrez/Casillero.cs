﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajedrez
{
    class Casillero
    {
        private int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        private Pieza pieza;

        public Pieza Pieza
        {
            get { return pieza; }
            set { pieza = value; }
        }

        private uiCasillero uicasillero;

        public uiCasillero UICasillero
        {
            get { return uicasillero; }
            set { uicasillero = value; }
        }

        public void ActualizarImagen() 
        {
            if (pieza != null) 
            { 
                uicasillero.Image = pieza.Imagen;
            }
            else
            {
                uicasillero.Image = null;
            }
        }
    }
}