﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Ajedrez
{
    public partial class AjedrezTab : Form
    {
        Juego juego = new Juego();
        Form3 loginref;
        Acceso acceso;
        bool ended= false;
        public AjedrezTab(Form3 login,String UserB, String UserN)
        {
            InitializeComponent();
            loginref = login;
            juego.UsernameBlancas = UserB;
            juego.UsernameNegras = UserN;
            acceso = loginref.acceso;
            label1.Text = juego.UsernameBlancas;
        }

        private void Ajedrez_Load(object sender, EventArgs e)
        {
            juego.Iniciar();
            Size tamaño = new Size(75, 75);

            listBox1.DataSource = null;
            listBox1.DataSource = juego.log;

            int modo = 0;
            foreach (Casillero cas in juego.Tablero.Casilleros)
            {
                uiCasillero casillero = new uiCasillero();
                casillero.Casillero = cas;
                casillero.Location = new Point(cas.X * 75 + cas.X+10, (7-cas.Y) * 75 + (7 - cas.Y) + 13);
                casillero.Size = tamaño;
                casillero.CambiarColor(modo);
                casillero.Click += Casillero_Click;
                this.Controls.Add(casillero);
                cas.UICasillero = casillero;
                if (modo == 0 && cas.X != 7)
                {
                    modo = 1;
                }
                else if (cas.X != 7)
                {
                    modo = 0;
                }
                EstablecerPiezaInicial(casillero, cas);
            }
        }

        private void EstablecerPiezaInicial(uiCasillero uicas, Casillero cas) 
        {
            if (cas.Y == 0) {
                if (cas.X == 1 || cas.X == 6) {
                    cas.Pieza = new Caballo(true);
                    cas.ActualizarImagen();
                }
                else if(cas.X == 2 || cas.X == 5) {
                    cas.Pieza = new Alfil(true);
                    cas.ActualizarImagen();
                }
                else if (cas.X == 3)
                {
                    cas.Pieza = new Reina(true);
                    cas.ActualizarImagen();
                }
                else if (cas.X == 0 || cas.X == 7)
                {
                    cas.Pieza = new Torre(true);
                    cas.ActualizarImagen();
                }
                else if (cas.X == 4)
                {
                    cas.Pieza = new Rey(true);
                    cas.ActualizarImagen();
                    juego.casreyb = cas;
                    juego.reybref = cas.Pieza;
                }

            }
            if (cas.Y == 7)
            {
                if (cas.X == 1 || cas.X == 6)
                {
                    cas.Pieza = new Caballo(false);
                    cas.ActualizarImagen();
                }
                else if (cas.X == 2 || cas.X == 5)
                {
                    cas.Pieza = new Alfil(false);
                    cas.ActualizarImagen();
                }
                else if (cas.X == 3)
                {
                    cas.Pieza = new Reina(false);
                    cas.ActualizarImagen();
                }
                else if (cas.X == 0 || cas.X == 7)
                {
                    cas.Pieza = new Torre(false);
                    cas.ActualizarImagen();
                }
                else if (cas.X == 4)
                {
                    cas.Pieza = new Rey(false);
                    cas.ActualizarImagen();
                    juego.casreyn = cas;
                    juego.reynref = cas.Pieza;
                }
            }
            if (cas.Y == 1)
            {
                cas.Pieza = new Peon(true);
                cas.ActualizarImagen();

            }
            if (cas.Y == 6)
            {
                cas.Pieza = new Peon(false);
                cas.ActualizarImagen();

            }
        }

        private void Casillero_Click(object sender, EventArgs e)
        {
            Casillero cas = ((uiCasillero)sender).Casillero;

            //MessageBox.Show((XEnum)cas.X + " "+ (cas.Y+1));
            juego.Click(cas);             

            listBox1.DataSource = null;
            listBox1.DataSource = juego.log;

            if (juego.JugandoBlancas) {
                label1.Text = juego.UsernameBlancas;
            }
            else
            {
                label1.Text = juego.UsernameNegras;
            }
            if (!ended && juego.log.Count>0 && juego.log.Last().Contains("Jaque Mate"))
            {
                Fin();              
            }
        }

        private void Fin()
        {
            ended = true;
            String Log= " (" + DateTime.Now.ToString()+") " +loginref.UsernameBlancas +" vs "+ loginref.UsernameNegras +" =>";
            foreach (String move in juego.log)
            {
                Log = Log + Environment.NewLine + move;
            }

            String WName;
            String LName;
            if (juego.log.Last().Contains("Negras"))
            {
                WName = loginref.UsernameBlancas;
                LName = loginref.UsernameNegras;
                MessageBox.Show(WName + " gano la partida!", "Victoria",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);

            }
            else
            {
                LName = loginref.UsernameBlancas;
                WName = loginref.UsernameNegras;
            }
            

            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@W", WName));
            string sql = "UPDATE UserTable SET wins = wins + 1 where username = @W";
            acceso.Abrir();
            acceso.Escribir(sql, parametros);
            
            parametros.Clear();

            parametros.Add(acceso.CrearParametro("@L", LName));
            sql = "UPDATE UserTable SET defeats = defeats + 1 where username = @L";
            
            acceso.Escribir(sql, parametros);                    
            parametros.Clear();


            List<SqlParameter> parametros2 = new List<SqlParameter>();
            parametros2.Add(acceso.CrearParametro("@W", WName));           
            sql = "SELECT * from UserTable where username = @W";
            SqlDataReader res =  acceso.Leer(sql, parametros2);
            while (res.Read())
            {
                WName=res[0].ToString();
            }
            res.Close();

            List<SqlParameter> parametros3 = new List<SqlParameter>();
            parametros3.Add(acceso.CrearParametro("@L", LName));
            sql = "SELECT * from UserTable where username = @L";
            SqlDataReader res2 = acceso.Leer(sql, parametros3);
            while (res2.Read())
            {
                LName = res2[0].ToString();
            }
            res2.Close();

            parametros.Add(acceso.CrearParametro("@W", WName));
            parametros.Add(acceso.CrearParametro("@L", LName));
            parametros.Add(acceso.CrearParametro("@ML",Log));
            parametros.Add(acceso.CrearParametro("@WCW",(!juego.JugandoBlancas ? 1 : 0).ToString() ));
            sql = "INSERT INTO Game ([winner],[loser],[match_log],[winner_color_white]) VALUES(@W, @L, @ML, @WCW)";
            acceso.Escribir(sql, parametros);
            acceso.Cerrar();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void AjedrezTab_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}

