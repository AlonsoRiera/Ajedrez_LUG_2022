﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ajedrez
{
    partial class Form2 : Form
    {
        Casillero cas;
        public Form2(Casillero casillero)
        {
            cas = casillero;
            InitializeComponent();
            comboBox1.Items.Add("Reina");
            comboBox1.Items.Add("Torre");
            comboBox1.Items.Add("Alfil");
            comboBox1.Items.Add("Caballo");
            comboBox1.Items.Add("Peon");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null)
            {
                MessageBox.Show("Elije una pieza");
            }
            else 
            {
                switch (comboBox1.SelectedItem)
                {
                    case "Reina":
                        Pieza tmp1 = new Reina(cas.Pieza.Blanca);
                        cas.Pieza = tmp1;
                        cas.ActualizarImagen();
                        break;
                    case "Torre":
                        Pieza tmp2 = new Torre(cas.Pieza.Blanca);
                        cas.Pieza = tmp2;
                        cas.ActualizarImagen();
                        break;
                    case "Alfil":
                        Pieza tmp3 = new Alfil(cas.Pieza.Blanca);
                        cas.Pieza = tmp3;
                        cas.ActualizarImagen();
                        break;
                    case "Caballo":
                        Pieza tmp4 = new Caballo(cas.Pieza.Blanca);
                        cas.Pieza = tmp4;
                        cas.ActualizarImagen();
                        break;
                    case "Peon":
                        Pieza tmp5 = new Peon(cas.Pieza.Blanca);
                        cas.Pieza = tmp5;
                        cas.ActualizarImagen();
                        
                        break;
                }
                this.Close();
            }
        }
    }
}
