﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ajedrez
{
    public partial class Form3 : Form
    {
        public Acceso acceso = new Acceso();

        public String UsernameBlancas = null;
        public String UsernameNegras = null;

        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            acceso.Abrir();
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@usu", textBox2.Text));
            string sql = "Select * from UserTable where BINARY username= @usu";


            SqlDataReader lector = acceso.Leer(sql, parametros);

            if (!lector.HasRows)
            {
                acceso.Cerrar();
                acceso.Abrir();
                parametros.Clear();
                parametros.Add(acceso.CrearParametro("@usu", textBox2.Text));
                parametros.Add(acceso.CrearParametro("@pass", textBox3.Text));
                sql = "INSERT INTO UserTable ([username],[password],[wins],[defeats]) VALUES(@usu, @pass, '0', '0')";
                acceso.Escribir(sql, parametros);
                acceso.Cerrar();
                MessageBox.Show("Usuario creado con exito!", "Uusario creado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            else
            {
                acceso.Cerrar();
                MessageBox.Show("El nombre de usuario ya esta usado, intente otro.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (!(UsernameBlancas == null || (UsernameBlancas != null && UsernameBlancas != textBox2.Text)))
            {
                MessageBox.Show("No puede ser el mismo usuario", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else { 
                try {
                    acceso.Abrir();
                    List<SqlParameter> parametros = new List<SqlParameter>();

                    parametros.Add(acceso.CrearParametro("@usu", textBox2.Text));
                    parametros.Add(acceso.CrearParametro("@pass", textBox3.Text));
                    string sql = "Select * from UserTable where username= @usu and password=@pass";



                    SqlDataReader lector = acceso.Leer(sql, parametros);

                    if (lector.HasRows)
                    {
                        acceso.Cerrar();
                        String welcomestr = "Bienvenido devuelta " + textBox2.Text + "!";
                        MessageBox.Show(welcomestr, "Usuario encontrado!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        if (UsernameBlancas == null)
                        {
                            UsernameBlancas = textBox2.Text;
                            button2.Text = "Login (Negras)";
                        }
                        else
                        {
                            UsernameNegras = textBox2.Text;
                            AjedrezTab juego = new AjedrezTab(this, UsernameBlancas, UsernameNegras);
                            juego.Show();
                            this.Hide();
                        }
                        textBox2.Text = "";
                        textBox3.Text = "";
                    }
                    else
                    {
                        acceso.Cerrar();
                        MessageBox.Show("Usuario o contraseña incorrecta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch
                {
                    MessageBox.Show("No se pudo acceder a la Base de Datos");
                }
            }
        }
    }   }
