﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Ajedrez
{
    class Juego
    {

        private Tablero tablero = new Tablero();

        public Tablero Tablero
        {
            get { return tablero; }
            set { tablero = value; }
        }

        // Esto se subira la BD como Historial de la partida
        public List<String> log = new List<string>();

        // Guarda si estan jugando las blancas (true) o las negras (false)
        private bool jugandoblancas = true;    

        public bool JugandoBlancas
        {
            get { return jugandoblancas; }
            set { jugandoblancas = value; }
        }

        // Cuenta si es el primer o segundo click para mover una pieza
        private int contadorclick = 0;  
        

        //Nota: Pense en guardar la informacion "repetida" de J1 y J2 en 2 instancias de una clase "Jugaador" pero me parecio mas sencillo asi y serian clases sin metodos
        public Casillero casreyb;
        public Casillero casreyn;

        public Pieza reybref;
        public Pieza reynref;

        public bool Jugador1enJaque;
        public bool Jugador2enJaque;

        public String UsernameBlancas;
        public String UsernameNegras;


        // Esto es para algunas operaciones de calculo y re-coloreo de casillero
        private Casillero casseleccionado;
        private Color colorcasseleccionado;

        // Lista de todos los movimientos posibles que puede hacer la ficha que se seleccionó
        private List<Casillero> posiblesmovimientos;

        public void Iniciar()
        {
            tablero.CrearTablero();
        }

        public void Click(Casillero cas) {

            if (contadorclick == 0)
            {              
                if (cas.Pieza != null && cas.Pieza.Blanca == jugandoblancas) // Si se selecciona una ficha del color del jugador que juega 
                {
                    posiblesmovimientos = cas.Pieza.PosiblesMovimientos(cas, tablero.Casilleros); //Se analizan los movs posibles de esa ficha
                    colorcasseleccionado = cas.UICasillero.BackColor;   //Se pinta el casillero seleccionado
                    cas.UICasillero.BackColor = Color.Yellow;
                    casseleccionado = cas;
                    foreach (Casillero poscasillero in posiblesmovimientos) // Se agrega el indicador de movimiento posible a todos los casilleros
                    {
                        if (poscasillero.Pieza == null)
                        {
                            poscasillero.UICasillero.Image = Ajedrez.Properties.Resources.dot;
                        }
                        else {
                            poscasillero.UICasillero.Image = Ajedrez.Properties.Resources.circle;
                            poscasillero.UICasillero.BackgroundImage = poscasillero.Pieza.Imagen;
                        }
                    }
                    
                    contadorclick++; // El siguiente click movera la ficha
                }
            }
            else if (contadorclick == 1)
            {
                if (posiblesmovimientos.Contains(cas)) // Si el segundo click esta en los casilleros a los que se podia mover esta ficha
                {
                    foreach (Casillero poscasillero in posiblesmovimientos) // Volvemos todos los casilleros a la normalidad
                    {
                        if (poscasillero.Pieza == null)
                        {
                            poscasillero.UICasillero.Image = null;
                        }
                        else
                        {
                            poscasillero.UICasillero.Image = poscasillero.Pieza.Imagen;
                            poscasillero.UICasillero.BackgroundImage = null;
                        }
                    }
                    cas.Pieza = casseleccionado.Pieza; //Comemos la pieza
                    casseleccionado.Pieza = null; //Borramos la referencia del casillero anterior
                    cas.ActualizarImagen();       // UI
                    casseleccionado.ActualizarImagen(); // UI
                    casseleccionado.UICasillero.BackColor = colorcasseleccionado;
                    contadorclick = 0; // Preparamos para el siguiente turno el click. Se hace aca porque si resulta en jaque mate se cambia a -1, impidiendo entrar en esta funcion

                    if (jugandoblancas) {  // Se registra el movimiento
                        log.Add(" "+ UsernameBlancas+" (B): "+(XEnum)casseleccionado.X + (casseleccionado.Y + 1) + " => " + (XEnum)cas.X + (cas.Y + 1));
                    }
                    else
                    {
                        log.Add(" " + UsernameNegras + " (N): " + (XEnum)casseleccionado.X + (casseleccionado.Y + 1) + " => " + (XEnum)cas.X + (cas.Y + 1));
                    }

                    if (cas.Pieza == reybref) // Si era el rey se ajusta la referencia del casillero rey
                    {
                        casreyb = cas;
                    }
                    else if (cas.Pieza == reynref)
                    {
                        casreyn = cas;
                    }

                   
                    var salidadea2lst = tablero.Casilleros.Where(casi => casi.Pieza !=null && casi.Pieza is Peon && (casi.Pieza as Peon).salidade2 == true);  // Esto es para la mecanica "Comer al Paso" 
                    foreach (Casillero p in salidadea2lst) // Apaga la flag de que salio a 2 casilleros el peon
                    {
                        (p.Pieza as Peon).salidade2 = false;
                    }


                    if (cas.Pieza is Peon)
                    {
                        Casillero tmp;
                        if (Math.Abs(cas.Y - casseleccionado.Y) == 2 && cas.X == casseleccionado.X) 
                        {                          
                            (cas.Pieza as Peon).salidade2 = true;     //Prende la flag de que salio a 2 casilleros el peon                  
                        }
                        else if (cas.X - 1 == casseleccionado.X) //Comer si fue un comer al paso
                        {                         
                            if (cas.Pieza.Blanca)
                            {
                                tmp = tablero.Casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X));
                            }
                            else
                            {
                                tmp = tablero.Casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == (cas.X));
                            }
                            tmp.Pieza = null;
                            tmp.ActualizarImagen();
                        }
                        else if (cas.X + 1 == casseleccionado.X)
                        {
                            if (cas.Pieza.Blanca)
                            {
                                tmp = tablero.Casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X));
                            }
                            else
                            {
                                tmp = tablero.Casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == (cas.X));
                            }
                            
                            tmp.Pieza = null;
                            tmp.ActualizarImagen();
                        }

                        if ((cas.Y == 7 && cas.Pieza.Blanca) || (cas.Y == 0 && !cas.Pieza.Blanca)) // Promocion del Peon
                        {
                            new Form2(cas).Show();
                        }
                        
                    }
                    
                    else  if (cas.Pieza is Rey && !cas.Pieza.movida && Math.Abs(cas.X - casseleccionado.X) == 2) //Logica del Enroque
                    {
                        if ((cas.X - casseleccionado.X) == 2)
                        {
                            if (cas.Pieza.Blanca)
                            {
                                Casillero torrecastmp = tablero.Casilleros.First(casi => casi.X == 7 && casi.Y == 0);
                                Casillero torretocastmp = tablero.Casilleros.First(casi => casi.X == 5 && casi.Y == 0);
                                torrecastmp.Pieza.movida=true;
                                torretocastmp.Pieza = torrecastmp.Pieza;
                                torrecastmp.Pieza = null;
                                torrecastmp.ActualizarImagen();
                                torretocastmp.ActualizarImagen();
                            }
                            else
                            {
                                Casillero torrecastmp = tablero.Casilleros.First(casi => casi.X == 7 && casi.Y == 7);
                                Casillero torretocastmp = tablero.Casilleros.First(casi => casi.X == 5 && casi.Y == 7);
                                torrecastmp.Pieza.movida = true;
                                torretocastmp.Pieza = torrecastmp.Pieza;
                                torrecastmp.Pieza = null;
                                torrecastmp.ActualizarImagen();
                                torretocastmp.ActualizarImagen();
                            }
                        }
                        else
                        {
                            if (cas.Pieza.Blanca)
                            {
                                Casillero torrecastmp = tablero.Casilleros.First(casi => casi.X == 0 && casi.Y == 0);
                                Casillero torretocastmp = tablero.Casilleros.First(casi => casi.X == 3 && casi.Y == 0);
                                torrecastmp.Pieza.movida = true;
                                torretocastmp.Pieza = torrecastmp.Pieza;
                                torrecastmp.Pieza = null;
                                torrecastmp.ActualizarImagen();
                                torretocastmp.ActualizarImagen();
                            }
                            else
                            {
                                Casillero torrecastmp = tablero.Casilleros.First(casi => casi.X == 0 && casi.Y == 7);
                                Casillero torretocastmp = tablero.Casilleros.First(casi => casi.X == 3 && casi.Y == 7);
                                torrecastmp.Pieza.movida = true;
                                torretocastmp.Pieza = torrecastmp.Pieza;
                                torrecastmp.Pieza = null;
                                torrecastmp.ActualizarImagen();
                                torretocastmp.ActualizarImagen();
                            }
                        }

                    }
                    
                    cas.Pieza.movida = true; // Le damos a la pieza que movimos arriba el flag de que se movio
                    jugandoblancas = !jugandoblancas; // Cambio el turno
                    casseleccionado = null;  //Vacio la referencia

                    if (Jugador1enJaque) // Si ya estaba en jaque
                    {
                        jugandoblancas = !jugandoblancas;
                        ChequearJaque(); 
                        if (Jugador1enJaque) // Y sigue en Jaque entonces es Jaque Mate
                        {
                            log.Add(" Jaque Mate a las Blancas");
                            contadorclick = -1;
                        }
                        else
                        {
                            jugandoblancas = !jugandoblancas;                        
                        }

                    }
                    else if (Jugador2enJaque)
                    {
                        jugandoblancas = !jugandoblancas;
                        ChequearJaque();
                        if (Jugador2enJaque)
                        {
                            log.Add(" Jaque Mate a las Negras");
                            contadorclick = -1;
                        }
                        else
                        {
                            jugandoblancas = !jugandoblancas;
                        }
                    }
                    else // Si no estaba en jaque, chequear Jaque
                    {
                        ChequearJaque();
                    }
                    
                }
                else if (cas == casseleccionado)  // Si el segundo click es al mismo casillero entonces se deselecciona y vuelve al click 0
                {
                    foreach (Casillero poscasillero in posiblesmovimientos)
                    {
                        if (poscasillero.Pieza == null)
                        {
                            poscasillero.UICasillero.Image = null;
                        }
                        else
                        {
                            poscasillero.UICasillero.Image = poscasillero.Pieza.Imagen;
                            poscasillero.UICasillero.BackgroundImage = null;
                        }
                    }
                    cas.UICasillero.BackColor = colorcasseleccionado;
                    casseleccionado = null;
                    contadorclick = 0;
                }

            }

        
        }

        public void ChequearJaque()
        {
            foreach (Casillero redcas in tablero.Casilleros)
            {
                if (redcas.UICasillero.BackColor == Color.Red)
                {
                    if ((redcas.X + redcas.Y) % 2 == 0)
                    {
                        redcas.UICasillero.CambiarColor(0);
                    }
                    else
                    {
                        redcas.UICasillero.CambiarColor(1);
                    }
                }
            }

            Casillero casrey = casreyb;
            if (!jugandoblancas)
            {
                casrey = casreyn;
            }
            bool alreadyinjaque1 = Jugador1enJaque;
            bool alreadyinjaque2 = Jugador2enJaque;
            Jugador1enJaque = false;
            Jugador2enJaque = false; 
            foreach (Casillero cas in tablero.Casilleros) //Busca en todos los casilleros
            {
               if ((cas.Pieza!=null && (cas.Pieza.Blanca != casrey.Pieza.Blanca))) // Donde haya piezas y sean de color contrario al rey que se esta analizando
               {
                   List<Casillero> temp = cas.Pieza.PosiblesMovimientos(cas, tablero.Casilleros); // Para cada pieza encontrada se analizan todos sus movimientos posibles
                   foreach (Casillero casposible in temp)
                   {
                       if (casposible == casrey) // Si alguna ficha puede moverse a donde esta el rey, entonces es Jaque
                        {
                            if (!jugandoblancas)
                            {
                                Jugador1enJaque = true;
                            }
                            else
                            {
                                Jugador2enJaque = true;
                            }
                            cas.UICasillero.CambiarColor(2);
                        }
                   }
               }             
            }
            if (Jugador1enJaque && !alreadyinjaque1)
            {
                log.Add(" Jaque a las Negras");
            }
            else if (Jugador2enJaque && !alreadyinjaque2)
            {
                log.Add(" Jaque a las Blancas");
            }

            casrey = casreyn;
            if (!jugandoblancas)
            {
                casrey = casreyb;
            }
            foreach (Casillero cas in tablero.Casilleros) //Busca en todos los casilleros
            {
                if ((cas.Pieza != null && (cas.Pieza.Blanca != casrey.Pieza.Blanca))) // Donde haya piezas y sean de color contrario al rey que se esta analizando
                {
                    List<Casillero> temp = cas.Pieza.PosiblesMovimientos(cas, tablero.Casilleros); // Para cada pieza encontrada se analizan todos sus movimientos posibles
                    foreach (Casillero casposible in temp)
                    {
                        if (casposible == casrey) // Si alguna ficha puede moverse a donde esta el rey, entonces es Jaque
                        {
                            if (!jugandoblancas && (!alreadyinjaque1))
                            {
                                log.Add(" Jaque Mate a las Blancas");
                                contadorclick = -1;
                            }
                            else if (jugandoblancas && (!alreadyinjaque2))
                            {
                                log.Add(" Jaque Mate a las Negras");
                                contadorclick = -1;
                            }
                            else if (!jugandoblancas && (alreadyinjaque1))
                            {
                                log.Add(" Jaque a las Blancas");
                            }
                            else if (jugandoblancas && (alreadyinjaque2))
                            {
                                log.Add(" Jaque a las Negras");
                            }
                            
                            cas.UICasillero.CambiarColor(2);                           
                        }
                    }
                }
            }



        }
    }
}
