﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Ajedrez
{
    class Peon : Pieza
    {
        public bool salidade2=false;
        public Peon(bool Blanca)
        {
            this.Blanca = Blanca;
            if (this.Blanca) 
            { 
                this.Imagen = Ajedrez.Properties.Resources.peon;
            }
            else
            {
                this.Imagen = Ajedrez.Properties.Resources.peonN;
            }
        }

        public override List<Casillero> PosiblesMovimientos(Casillero cas, List<Casillero> casilleros)
        {
            List<Casillero> posiblesmovimientos = new List<Casillero>();
            if (Blanca) { 
                if (!movida)
                {   
                    posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 2) && casilla.X == cas.X && casilla.Pieza==null));
                }

                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == cas.X && casilla.Pieza == null));

                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == (cas.X + 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == (cas.X - 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca));

                Casillero tmp = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y) && casilla.X == (cas.X - 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca && casilla.Pieza is Peon && (casilla.Pieza as Peon).salidade2==true);
                if (tmp != null)
                {
                    posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 2) && casilla.X == (cas.X - 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca));
                }

                tmp = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y) && casilla.X == (cas.X + 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca && casilla.Pieza is Peon);
                if (tmp != null)
                {
                    posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 2) && casilla.X == (cas.X + 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca));
                }

            }
            else
            {
                if (!movida)
                {
                    posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 2) && casilla.X == cas.X && casilla.Pieza == null));
                }

                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == cas.X && casilla.Pieza == null));

                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X + 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca));
                posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X - 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca));

                Casillero tmp = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y) && casilla.X == (cas.X - 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca && casilla.Pieza is Peon && (casilla.Pieza as Peon).salidade2 == true);
                if (tmp != null)
                {
                    posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X - 1) && casilla.Pieza == null));
                }

                tmp = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y) && casilla.X == (cas.X + 1) && casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca && casilla.Pieza is Peon);
                if (tmp != null)
                {
                    posiblesmovimientos.Add(casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X + 1) && casilla.Pieza == null));
                }
            }
            
            return posiblesmovimientos.Where(x => x != null).ToList(); 
        }
    }
}