﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Ajedrez
{
    abstract class Pieza
    {

        public bool movida = false;

        private bool blanca;

        public bool Blanca
        {
            get { return blanca; }
            set { blanca = value; }
        }

        private Image imagen;

        public Image Imagen
        {
            get { return imagen; }
            set { imagen = value; }
        }


        public virtual List<Casillero> PosiblesMovimientos(Casillero cas, List<Casillero> casilleros) { return casilleros; }
    }
}