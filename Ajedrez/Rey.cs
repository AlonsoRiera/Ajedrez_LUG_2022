﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ajedrez
{
    class Rey : Pieza
    {
        public Rey(bool Blanca)
        {
            this.Blanca = Blanca;
            if (this.Blanca)
            {
                this.Imagen = Ajedrez.Properties.Resources.wk;
            }
            else
            {
                this.Imagen = Ajedrez.Properties.Resources.bk;
            }
        }

        public override List<Casillero> PosiblesMovimientos(Casillero cas, List<Casillero> casilleros)
        {
            List<Casillero> posiblesmovimientos = new List<Casillero>();

            Casillero tempcas;



            tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == cas.X && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
            posiblesmovimientos.Add(tempcas);

            tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == cas.X && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
            posiblesmovimientos.Add(tempcas);

            tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X + 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
            posiblesmovimientos.Add(tempcas);

            tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X - 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
            posiblesmovimientos.Add(tempcas);



            tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == (cas.X + 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
            posiblesmovimientos.Add(tempcas);

            tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X - 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
            posiblesmovimientos.Add(tempcas);

            tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y + 1) && casilla.X == (cas.X - 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
            posiblesmovimientos.Add(tempcas);

            tempcas = casilleros.FirstOrDefault(casilla => casilla.Y == (cas.Y - 1) && casilla.X == (cas.X + 1) && (casilla.Pieza == null || (casilla.Pieza != null && casilla.Pieza.Blanca != cas.Pieza.Blanca)));
            posiblesmovimientos.Add(tempcas);

            if (!movida) //Enroque
            {
                Casillero tempcas1 = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X + 1) && (casilla.Pieza == null));
                Casillero tempcas2 = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X + 2) && (casilla.Pieza == null));
                Casillero tempcas3 = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X + 3) && (casilla.Pieza != null && casilla.Pieza is Torre && casilla.Pieza.movida == false));
                if (tempcas1 != null && tempcas2 != null && tempcas3 != null)
                {
                    posiblesmovimientos.Add(tempcas2);
                }
                tempcas1 = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X - 1) && (casilla.Pieza == null));
                tempcas2 = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X - 2) && (casilla.Pieza == null));
                tempcas3 = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X - 3) && (casilla.Pieza == null));
                Casillero tempcas4 = casilleros.FirstOrDefault(casilla => casilla.Y == cas.Y && casilla.X == (cas.X - 4) && (casilla.Pieza != null && casilla.Pieza is Torre && casilla.Pieza.movida == false));
                if (tempcas1 != null && tempcas2 != null && tempcas3 != null && tempcas3 != null)
                {
                    posiblesmovimientos.Add(tempcas2);
                }

            }
            return posiblesmovimientos.Where(x => x != null).ToList();
        }
    }
}