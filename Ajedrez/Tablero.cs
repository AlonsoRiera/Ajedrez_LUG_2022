﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ajedrez
{
    class Tablero
    {
        private List<Casillero> casilleros = new List<Casillero>();

        public List<Casillero> Casilleros
        {
            get { return casilleros; }

        }

        public void CrearTablero()
        {
            for (int fila = 0; fila < 8; fila++)
            {
                for (int col = 0; col < 8; col++)
                {                  
                 Casillero cas = new Casillero();
                 cas.X = col;
                 cas.Y = fila;
                 Casilleros.Add(cas);
                }

            }
        }


    }
}