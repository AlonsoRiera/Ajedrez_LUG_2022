﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Ajedrez
{
    class uiCasillero : PictureBox
    {
        public uiCasillero()
        {
            this.BackColor = Color.Black;
        }

        private Casillero casillero;

        public Casillero Casillero
        {
            get { return casillero; }
            set { casillero = value; }
        }


        public void CambiarColor(int modo)
        {
            if (modo == 0)
            {
                this.BackColor = Color.FromArgb(255, 118, 150, 86);
            }
            else if (modo == 1)
            {
                this.BackColor = Color.FromArgb(255, 238, 238, 210);
            }
            else if (modo == 2)
            {
                this.BackColor = Color.Red;
            }
        }


    }
}